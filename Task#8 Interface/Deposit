package hometask8;

import java.math.BigDecimal;


public abstract class Deposit implements Comparable<Deposit> {

    public BigDecimal amount;
    protected int period;

    public BigDecimal amount() {
        return amount;
    }

    public int period() {
        return period;
    }

    public Deposit(BigDecimal depositAmount, int depositPeriod) {
        this.amount = depositAmount;
        this.period = depositPeriod;
    }

    public abstract BigDecimal income();

    @Override
    public int compareTo(Deposit o) {
        BigDecimal currentValue = amount().add(income());
        BigDecimal otherValue = o.income().add(o.amount());
        if (currentValue.compareTo(otherValue) > 0) return 1;
        else if (currentValue.compareTo(otherValue) == 0) return 0;
        else return -1;
    }
}
