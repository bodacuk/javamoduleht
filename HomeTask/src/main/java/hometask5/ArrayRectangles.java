package hometask5;

public class ArrayRectangles {



    private Rectangle[] rectangleArray;

    public ArrayRectangles(int n)
    {
        rectangleArray = new Rectangle[n];
    }

    public ArrayRectangles(Rectangle ... rectangle) {
        this.rectangleArray = rectangle;
    }

    public boolean addRectangle(Rectangle rectangle) {
        if (rectangle == null)
        {
            throw new NullPointerException();
        }
        for (int i = 0; i < rectangleArray.length; i++)
        {
            if (rectangleArray[i] == null )
            {
                rectangleArray[i]= rectangle;
                return true;
            }
        }
        return false;
    }


    public int numberMaxArea() {
        int maxArea = (int) rectangleArray[0].area();
        int index = 0;

        if (rectangleArray == null)
        {
            throw new NullPointerException();
        }

        for (int i = 0; i < rectangleArray.length; i++) {
            if(rectangleArray[i] != null && maxArea<rectangleArray[i].area())
            {
                maxArea = (int) rectangleArray[i].area();
                index = i;
            }
        }
        return index;
    }


    public int numberMinPerimeter() {
        int minPerimeter = (int) rectangleArray[0].perimeter();
        int index = 0;

        if (rectangleArray == null)
        {
            throw new NullPointerException();
        }
        for (int i = 0; i < rectangleArray.length; i++) {
            if(rectangleArray[i] != null && minPerimeter>rectangleArray[i].perimeter())
            {
                minPerimeter = (int) rectangleArray[i].perimeter();
                index = i;
            }

        }
        return index;
    }

    public int numberSquares() {
        int number = 0;
        if (rectangleArray == null)
        {
            throw new NullPointerException();
        }
        for (int i = 0; i < rectangleArray.length; i++) {
            if (rectangleArray[i].isSquare()){
                number++;
            }

        }
        return number;
    }

}