package hometask5;

public class Rectangle {
    private double sideA;
    private double sideB;

    public Rectangle(double a, double b) {
        this.sideA = a;
        this.sideB = b;
    }

    public Rectangle(double a) {
        this.sideA = a;
        this.sideB = 5;
    }

    public Rectangle() {
        this.sideA = 4;
        this.sideB = 3;
    }

    public double getSideA() {
        if (sideA <= 0) {
            throw new IllegalArgumentException();
        } else return sideA;
    }

    public double getSideB() {
        if (sideB <= 0) {
            throw new IllegalArgumentException();
        } else return sideB;
    }

    public double area() {
        if (sideA <= 0 || sideB <= 0) {
            throw new UnsupportedOperationException();
        } else {
            return sideA * sideB;
        }
    }

    public double perimeter() {
        if (sideB <= 0 || sideA <= 0) {
            throw new UnsupportedOperationException();
        } else {
            return (sideA + sideB) * 2;
        }
    }

    public boolean isSquare() {
        if (sideB <= 0 || sideA <= 0) {
            throw new UnsupportedOperationException();
        } else {
            return sideB == sideA;
        }
    }

    public void replaceSides() {
        if (sideB <= 0 || sideA <= 0) {
            throw new UnsupportedOperationException();
        } else {
            double swap = sideA;
            sideA = sideB;
            sideB = swap;
        }
    }
}
