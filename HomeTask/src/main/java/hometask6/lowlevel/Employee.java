package hometask6.lowlevel;

import java.math.BigDecimal;

public abstract class Employee {
    private String name;
    private BigDecimal salary;
    private BigDecimal bonus;

    public Employee(String name, BigDecimal salary) {
        this.name = name;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getSalary() {
        return salary;
    }


    public BigDecimal getBonus() {
        return bonus;
    }

    public abstract void setBonus(BigDecimal bonus);

    public BigDecimal toPay () {
        return salary.add(bonus);
    }
}
