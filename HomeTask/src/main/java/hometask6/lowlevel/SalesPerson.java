package hometask6.lowlevel;

import java.math.BigDecimal;

public class SalesPerson extends  Employee {
    private int percent;

    public SalesPerson(String name, BigDecimal salary, int percent) {
        super(name, salary);
        this.percent = percent;
    }

    public int getPercent() {
        return percent;
    }

    @Override
    public void setBonus(BigDecimal bonus) {
        if (percent> 100){
            bonus.multiply(BigDecimal.valueOf(2));
        } else if (percent > 200){
            bonus.multiply(BigDecimal.valueOf(3));
        }

    }
}
