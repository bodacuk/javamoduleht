package hometask6.lowlevel;
import java.math.BigDecimal;

public class Manager extends Employee {
    private int quantity;

    public Manager(String name, BigDecimal salary, int quantity) {
        super(name, salary);
        this.quantity = quantity;
    }

    @Override
    public void setBonus(BigDecimal bonus) {
        if (quantity > 100) {
            bonus = bonus.add(BigDecimal.valueOf(500));
        } else if (quantity > 150) {
            bonus = bonus.add(BigDecimal.valueOf(1000));
        } else bonus = BigDecimal.ZERO;
    }
}
