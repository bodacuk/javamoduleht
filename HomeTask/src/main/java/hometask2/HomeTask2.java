package hometask2;

public class HomeTask2 {
    public static int task1(int value) {

        if (value > 0) {
            String s = Integer.toString(value);
            int[] count = new int[s.length()];
            for (int i = 0; i < s.length(); i++) {
                count[i] = s.charAt(i) - '0';
            }
            int res = 0;
            for (int i = 0; i < count.length; i++) {
                if (count[i] % 2 != 0) {
                    res = res + count[i];
                }
            }
            return res;
        } else throw new IllegalArgumentException();
    }

    public static int task2(int value) {
        String res = Integer.toBinaryString(value);
        int[] count = new int[res.length()];
        if (value > 0) {
            for (int i = 0; i < res.length(); i++) {
                count[i] = res.charAt(i) - '0';
            }
            int result = 0;
            for (int i = 0; i < count.length; i++) {
                if (count[i] % 2 != 0)
                    result = count[i] + result;
            }
            return result;
        } else throw new IllegalArgumentException();
    }

    public static int task3(int value) {
        int result = 0;
        int first = 0;
        int second = 1;
        int third;
        if (value <= 0) {
            throw new IllegalArgumentException();
        } else if (value == 1) {
            return result;
        } else if (value == 2) {
            return result = 1;
        } else {
            for (int i = 2; i < value; i++) {
                third = first + second;
                result = result + third;
                first = second;
                second = third;

            }
            result += 1;

        }
        return result;
    }
}