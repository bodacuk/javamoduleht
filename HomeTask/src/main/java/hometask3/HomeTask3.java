package hometask3;

public class HomeTask3 {
    public static int[] task1(int[] array) {
        if (array == null) {
            throw new UnsupportedOperationException();
        } else {
            int lastIndex = array.length - 1;
            for (int i = 0; i < array.length; i++) {
                int pairIndex = lastIndex - i;
                if (array[i] == 0) {
                    throw new UnsupportedOperationException();
                } else if (pairIndex > i && array[i] % 2 == 0 && array[pairIndex] % 2 == 0) {
                    int swap = array[i];
                    array[i] = array[pairIndex];
                    array[pairIndex] = swap;
                }
            }
            return array;
        }
    }

    public static int task2(int[] array) {
        int result = 0;
        if (array == null) {
            throw new UnsupportedOperationException();
        } else {
            int maxValue = array[0];
            int maxIndex = 0;
            int maxIndex2 = 0;
            for (int i = 0; i < array.length; i++) {
                if (maxValue < array[i]) {
                    maxValue = array[i];
                    maxIndex = i;
                } else if (maxValue == array[i]) {
                    maxIndex2 = i;
                }
                if (maxIndex2 > 0) {
                    result = maxIndex2 - maxIndex;
                }
            }
            return result;
        }
    }

    public static int[][] task3(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = i - 1; j >= 0; j--) {
                matrix[j][i] = 1;
            }
        }
        for (int i = 0; i < matrix.length; i++) {
            for (int j = i - 1; j >= 0; j--) {
                matrix[i][j] = 0;
            }
        }
        return matrix;
    }
}