package hometask9;


public class Matrix {

    private int row;
    private int column;
    private double[][] matrix;


    public Matrix(int row, int column) throws MatrixException {

        if (row <= 0 || column <= 0) throw new MatrixException("Matrix is not correct");
        else {
            this.row = row;
            this.column = column;
            double[][] twoDimensionalArray = new double[row][column];

            for (int i = 0; i < row; i++) {
                for (int j = 0; j < column; j++) {
                    twoDimensionalArray[i][j] = 0;
                }
            }
        }
    }

    public Matrix(double[][] twoDimensionalArray) throws MatrixException {
        if (twoDimensionalArray.length <= 0) {
            throw new MatrixException("Array passed with zero number of rows");
        } else if (twoDimensionalArray[0].length <= 0) {
            throw new MatrixException("Array passed with zero number of columns");
        }
        for (int i = 0; i < twoDimensionalArray.length; i++) {
            for (int t = 0; t < twoDimensionalArray[0].length; t++) {
                this.matrix[i][t] = twoDimensionalArray[i][t];
            }
        }
    }

    public final int ROWS() {
        if (row <= 0) {
            throw new UnsupportedOperationException();
        }
        return row;

    }

    public final int COLUMNS() {
        if (column <= 0) {
            throw new UnsupportedOperationException();
        }
        return column;
    }

    public double[][] getMatrix() {
        return matrix;
    }

    public void setMatrix(double[][] matrix) {
        this.matrix = matrix;
    }

    /**
     * Receiving of standard two-dimensional array out of matrix.
     *
     * @return Standard two-dimensional array
     */
    public double[][] twoDimensionalArrayOutOfMatrix() {
        //TODO: Delete line below and write your own solution;
        throw new UnsupportedOperationException();
    }

    public double getValue(int row, int column) throws MatrixException {
        if (row < 0 || column < 0) {
            throw new MatrixException("Incompatible matrix sizes");
        }
        return matrix[row][column];
    }

    public void setValue(int row, int column, double newValue) throws MatrixException {
        if (row < 0 || column < 0) {
            throw new MatrixException("Incompatible matrix sizes");
        }
        matrix[row][column] = newValue;
    }


    /**
     * Method of matrix's addition  <code>matrix</code>.
     * Result in the original matrix
     *
     * @param matrix matrix corresponding to the second term
     * @return Returns a new resulting matrix
     * @throws MatrixException if incompatible matrix sizes, returns message "Incompatible matrix sizes"
     */
    public Matrix addition(Matrix matrix) throws MatrixException {
        if (matrix.column < 0 || matrix.row < 0) {
            throw new MatrixException("Incompatible matrix sizes");
        }
        for (int i = 0; i < matrix.row; i++) {
            for (int j = 0; j < matrix.row; j++) {
                return this.matrix[i][j] = this.matrix[i][j] + matrix[i][j];
            }
        }

    }

    /**
     * Method of matrix's deduction <code>matrix</code> from original.
     * Result in the original matrix
     *
     * @param matrix matrix corresponding to the subtracted
     * @return Returns a new resulting matrix
     * @throws MatrixException if incompatible matrix sizes, returns message "Incompatible matrix sizes"
     */
    public Matrix subtraction(final Matrix matrix) throws MatrixException {
        if (matrix.column < 0 || matrix.row < 0) {
            throw new MatrixException("Incompatible matrix sizes");
        }
        for (int i = 0; i < matrix.row; i++) {
            for (int j = 0; j < matrix.row; j++) {
                return this.matrix[i][j] - matrix[i][j];
            }
        }
    }

    /**
     * Method of matrix's multiplication <code>matrix</code>
     * Result in the original matrix
     *
     * @param matrix matrix corresponding to the second factor
     * @return Returns a new resulting matrix
     * @throws MatrixException if incompatible matrix sizes, returns message "Incompatible matrix sizes"
     */
    public Matrix multiplication(final Matrix matrix) throws MatrixException {
        if (matrix.column < 0 || matrix.row < 0) {
            throw new MatrixException("Incompatible matrix sizes");
        }
        for (int i = 0; i < matrix.row; i++) {
            for (int j = 0; j < matrix.row; j++) {
                return this.matrix[i][j] * matrix[i][j];
            }
        }

  }
