package hometask9;

public class MatrixException extends Exception {
    public MatrixException(String message) {
        super(message);
    }
}
