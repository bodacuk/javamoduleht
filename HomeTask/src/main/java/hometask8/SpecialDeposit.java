package hometask8;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class SpecialDeposit extends Deposit implements Prolongable {
    public SpecialDeposit(BigDecimal depositAmount, int depositPeriod) {
        super(depositAmount, depositPeriod);
    }

    @Override
    public BigDecimal income() {
        BigDecimal value = amount();
        BigDecimal percent = BigDecimal.valueOf(1.01);
        MathContext m;
        for (int i = 0; i < period(); i++) {

            value = value.multiply(percent);
            percent = percent.add(BigDecimal.valueOf(0.01));

        }
        value = value.subtract(amount());
        m = new MathContext(2 + value.precision() - value.scale(), RoundingMode.HALF_EVEN);
        return value.round(m);

    }


    @Override
    public boolean canToProlong() {

        if (amount().compareTo(BigDecimal.valueOf(1000)) > 0) {
            return true;
        }
        return false;
    }
}