package hometask8;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class Client implements Iterable<Deposit> {
    private Deposit[] deposits;

    public Client() {
        deposits = new Deposit[10];
    }

    public boolean addDeposit(Deposit deposit) {

        if (deposits.length <= 0) {
            throw new IllegalArgumentException();
        }
        for (int i = 0; i < deposits.length; i++) {
            if (deposits[i] == null) {
                deposits[i] = deposit;
                return true;
            }
        }
        return false;
    }

    public BigDecimal totalIncome() {
        BigDecimal total = deposits[0].income();
        for (int i = 1; i < deposits.length; i++)
            if ((deposits.length >= i && deposits[i] != null)) {
                total = total.add(deposits[i].income());
            }
        return total;
    }

    public BigDecimal maxIncome() {
        BigDecimal maxIncome = this.deposits[0].income();

        for (int i = 1; i < this.deposits.length; i++) {
            if (deposits[i] != null) {
                if (deposits[i].income().compareTo(maxIncome) > 0) ;
                {
                    maxIncome = deposits[i].income();

                }
            }
        }
        return maxIncome;
    }

    public BigDecimal getIncomeByNumber(int number) {


        if (deposits.length >= number && deposits[number] != null) {
            return deposits[number].income();
        }
        return BigDecimal.ZERO;
    }

    public void sortDeposits() {
        if (deposits == null) {
            throw new NullPointerException();
        } else Arrays.sort(deposits);
    }


    public int countPossibleToProlongDeposit() {
        int i = 0;
        for (Deposit d: deposits) {
            if (d.canToProlong()){
                i++;
            }
        }
        return i;
    }


    @Override
    public Iterator<Deposit> iterator() {

        Iterator<Deposit> it = new Iterator<Deposit>() {
            int position = 0;

            @Override
            public boolean hasNext() {
                if (deposits[position] == null) {
                    return false;
                }
                return true;
            }

            @Override
            public Deposit next() {
                if (deposits[position] == null) {
                    throw new NoSuchElementException();
                }
                return deposits[position++];

            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("Cannot remove an element of an array.");
            }

        };
        return it;
    }
}