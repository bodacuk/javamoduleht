package hometask8;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class LongDeposit extends Deposit implements Prolongable {

    public LongDeposit(BigDecimal depositAmount, int depositPeriod) {
        super(depositAmount, depositPeriod);
    }

    @Override
    public BigDecimal income() {
        BigDecimal value = amount();
        BigDecimal percent = BigDecimal.valueOf(1.15);
        MathContext m;

        if (period() > 6) {
            for (int i = 6; i < period(); i++) {
                value = value.multiply(percent);

            }
        }
        value = value.subtract(amount());
        m = new MathContext(2 + value.precision() - value.scale(), RoundingMode.HALF_EVEN);
        return value.round(m);

    }


    public boolean canToProlong() {
        if (period() < 36) {
            return true;
        }
        return false;
    }

}
