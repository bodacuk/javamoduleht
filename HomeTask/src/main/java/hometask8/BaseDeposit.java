package hometask8;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class BaseDeposit extends Deposit {
    public BaseDeposit(BigDecimal depositAmount, int depositPeriod) {
        super(depositAmount, depositPeriod);
    }

    @Override
    public BigDecimal income() {
        BigDecimal result = amount();
        BigDecimal percent = BigDecimal.valueOf(1.05);
        MathContext m;
        for (int i = 0; i < period(); i++) {
            result = result.multiply(percent);
        }
        result = result.subtract(amount());
        m = new MathContext(2 + result.precision() - result.scale(), RoundingMode.HALF_EVEN);
        return result.round(m);

    }

}
