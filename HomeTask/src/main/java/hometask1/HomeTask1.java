package hometask1;

import java.util.Arrays;

public class HomeTask1 {
    public static int task1(int n) {
        if (n > 0) {
            return n * n;
        } else if (n == 0) {
            return 0;
        } else {
            return -n;
        }
    }

    public static int task2(int n) {
        if (n < 100 && n > 999) {
            throw new UnsupportedOperationException();
        } else {
            int nLast = n % 10;
            int nPrevious = n / 10 % 10;
            int first = n / 100 % 10;
            int[] arr = new int[]{nLast, nPrevious, first};
            Arrays.sort(arr);
            first = arr[2];
            nPrevious = arr[1];
            nLast = arr[0];
            int result = first * 100 + nPrevious * 10 + nLast;
            return result;
        }

    }
}

