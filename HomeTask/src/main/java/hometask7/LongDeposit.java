package hometask7;

import java.math.BigDecimal;

public class LongDeposit extends Deposit{
    public LongDeposit(BigDecimal depositAmount, int depositPeriod) {
        super(depositAmount, depositPeriod);
    }

    @Override
    public BigDecimal income() {
        BigDecimal result = getAmount();
        if (getPeriod()<7){
            return getAmount();
        } else if (getPeriod()>6){
            for (int i = 6; i < getPeriod() ; i++) {
                result = result.multiply(BigDecimal.valueOf(1.15));
            }
        }
        return result;
    }
}
