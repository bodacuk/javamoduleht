package hometask7;

import java.math.BigDecimal;

public class SpecialDeposit extends Deposit {

    public SpecialDeposit(BigDecimal depositAmount, int depositPeriod) {
        super(depositAmount, depositPeriod);
    }


    @Override
    public BigDecimal income() {

        BigDecimal result = getAmount();
        BigDecimal percent = BigDecimal.valueOf(1.01);
        for (int i = 0; i < getPeriod(); i++) {
            result = result.multiply(percent);
            percent= percent.add(BigDecimal.valueOf(0.01));
        }
        return result;
    }
}
