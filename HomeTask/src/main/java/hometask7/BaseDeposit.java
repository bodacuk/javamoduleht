package hometask7;

import java.math.BigDecimal;

public class BaseDeposit extends Deposit{

    public BaseDeposit(BigDecimal depositAmount, int depositPeriod) {
        super(depositAmount, depositPeriod);
    }

    @Override
    public BigDecimal income() {
        BigDecimal amount = getAmount();
        for (int i = 0; i < getPeriod(); i++) {

            amount = amount.multiply(BigDecimal.valueOf(1.05));

        }
        return amount;
    }
}