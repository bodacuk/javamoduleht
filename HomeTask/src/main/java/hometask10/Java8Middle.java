package hometask10;

import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Java8Middle {


    // Task 6
    public static List<String> getListOfStringsFromListOfIntegerAndStrings(List<Integer> integers, List<String> stringList) {
        return integers.stream()
                .map(i -> stringList.stream()
                        .filter(s -> s.length() == i && i > 0 && Character.isDigit(s.charAt(0)))
                        .filter(s -> s.length() == i && s.matches("\\d.*"))
                        .findFirst()
                        .orElse("Not Found")
                )
                .collect(Collectors.toList());
    }

    // Task 7
    public static List<Integer> subsetOfIntegers(int k, List<Integer> integerList) {

        List<Integer> res1 = integerList
                .stream()
                .limit(k)
                .filter(i -> i % 2 == 0)
                .collect(Collectors.toList());
        List<Integer> res2 = integerList
                .stream()
                .skip(k)
                .filter(i -> i % 2 == 1)
                .collect(Collectors.toList());
        List<Integer> result = Stream.concat(res1.stream(), res2.stream())
                .sorted(Collections.reverseOrder())
                .collect(Collectors.toList());
        return result;
    }

    // Task 8
    public static List<Integer> subsetOfTwoSubsetsIntegers(int k, int d, List<Integer> integerList) {
        List<Integer> res1 = integerList.stream().filter(i -> i > d).collect(Collectors.toList());
        List<Integer> res2 = integerList.stream().skip(k).collect(Collectors.toList());
        List<Integer> result = Stream.concat(res1.stream(), res2.stream())
                .distinct()
                .sorted(Collections.reverseOrder())
                .collect(Collectors.toList());
        return result;
    }

    //Task 9
    public static List<String> sizeAndFirstLetterOfWordsStartingWithTheSameLetter(List<String> stringList) {
        return stringList.stream()
                .map(string -> new AbstractMap.SimpleEntry<>(string.charAt(0), string.length()))
                .collect(
                        Collectors.collectingAndThen(Collector.of((Supplier<HashMap<Character, Integer>>) HashMap::new,
                                        (map, entry) -> {
                                            int prevSize = map.getOrDefault(entry.getKey(), 0);
                                            map.put(entry.getKey(), prevSize + entry.getValue());
                                        },
                                        (a, b) -> {
                                            throw new UnsupportedOperationException();
                                        }),
                                map -> map.entrySet()
                                        .stream()))
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .map(entry -> entry.getValue() + "-" + entry.getKey())
                .collect(Collectors.toList());

    }
    //Task 10
    public static List<Character> characterUpperCaseList(List<String> stringList){
        return stringList.stream().sorted(Collections.reverseOrder())
                .map(p -> p.toUpperCase())
                .map(m -> m.charAt(m.length()-1))
                .collect(Collectors.toList());
    }
}
