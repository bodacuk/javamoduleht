package hometask10;

public class YearSchoolStat {
    private int year_of_entering;
    public int number_of_schools;

    public YearSchoolStat(int year_of_entering, int number_of_schools) {
        this.year_of_entering = year_of_entering;
        this.number_of_schools = number_of_schools;
    }

    public int getYear_of_entering() {
        return year_of_entering;
    }

    public void setYear_of_entering(int year_of_entering) {
        this.year_of_entering = year_of_entering;
    }

    public int getNumber_of_schools() {
        return number_of_schools;
    }

    public void setNumber_of_schools(int number_of_schools) {
        this.number_of_schools = number_of_schools;
    }

    @Override
    public String toString() {
        return "YearSchoolStat{" +
                "year_of_entering=" + year_of_entering +
                ", number_of_schools=" + number_of_schools +
                '}';
    }
}
