package hometask10;

import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;

public class Entrant  {
    private int school_number;
    private int year_of_entering;
    private String  last_name;

    public Entrant(int chool_number, int year_of_entering, String last_name)  {
        this.school_number = chool_number;
        this.year_of_entering = year_of_entering;
        this.last_name = last_name;
    }

    public int getChool_number() {
        return school_number;
    }

    public void setChool_number(int chool_number) {
        this.school_number = chool_number;
    }

    public int getYear_of_entering() {
        return year_of_entering;
    }

    public void setYear_of_entering(int year_of_entering) {
        this.year_of_entering = year_of_entering;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    @Override
    public String toString() {
        return "Entrant{" +
                "chool_number=" + school_number +
                ", year_of_entering=" + year_of_entering +
                ", last_name='" + last_name + '\'' +
                '}';
    }


}
