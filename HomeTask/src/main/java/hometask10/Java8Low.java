package hometask10;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Java8Low {

    // Task 1
    public static List<String> getStringList(String c, List<String> stringList) {
        return stringList.stream().filter(string -> string.endsWith(c) && string.startsWith(c))
                .collect(Collectors.toList());
    }

    //Task 2

    public static List<Integer> stringslengths (List<String> stringList){
        Comparator<String> compByLength = (aName, bName) -> aName.length() - bName.length();
        return stringList.stream().
                sorted(compByLength).map(String::length).
                collect(Collectors.toList());

    }
    //Task 3.
    public static List<String> getFirstAndLastChar (List<String>stringList){
        return stringList.stream().map(str -> str.substring(0,1)+str.substring(str.length()-1))
                .collect(Collectors.toList());
    }

   // Task 4.
    public static List<String> getStringWithLenghtKAndEndingInDigit(int k, List<String> stringList) {
        return stringList.stream().
                filter(str -> str.length()==k && Character.isDigit(str.charAt(str.length()-1))).
                collect(Collectors.toList());
    }
//    Task 5.
    public static List<String> getOddIntegerList (List<Integer> integerList){
        return integerList.stream().filter(num -> num % 2 != 0).
                sorted().map(str -> str.toString()).
                collect(Collectors.toList());
    }

}
